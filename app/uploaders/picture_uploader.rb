class PictureUploader < CarrierWave::Uploader::Base
  include CarrierWave::MiniMagick
  process resize_to_fill: [200, 200, 'center']
  storage :file
  

  def store_dir
    "uploads/#{model.class.to_s.underscore}/#{mounted_as}/#{model.id}"
  end

  def extension_whitelist
    %w(jpeg png)
  end
  
  def create_square_image(magick, size)
    narrow = magick[:width] > magick[:height] ? magick[:height] : magick[:width]
    magick.combine_options do |c|
     c.gravity "center"
     c.crop "#{narrow}x#{narrow}+0+0"
    end
    magick.resize "#{size}x#{size}"
  end

end
